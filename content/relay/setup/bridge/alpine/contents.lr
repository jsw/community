_model: page
---
color: primary
---
title: Alpine Linux
---
body:

### 1. Enable Automatic Software Updates

One of the most important things to keep your relay secure is to install security updates timely and ideally automatically so you can not forget about it.
Follow the instructions to enable automatic software updates for your operating system.

### 2. Install `tor`

To install the `tor` package on Alpine Linux, please run:

```
# apk add tor
```

### 3. Install a Pluggable Transport

We are opting here to install and use `obfs4` as pluggable transport, so we are going to install `obfs4proxy`.

```
# doas apk add git
# git clone https://gitlab.com/yawning/obfs4.git
# cd obfs4/obfs4proxy
# go build
# doas mv obfs4proxy /usr/bin/obfs4proxy
```

### 4. Edit your Tor config file, usually located at `/etc/tor/torrc` and replace its content with:

```
BridgeRelay 1
DataDirectory /var/lib/tor
User tor

# Replace "TODO1" with a Tor port of your choice.  This port must be externally
# reachable.  Avoid port 9001 because it's commonly associated with Tor and
# censors may be scanning the Internet for this port.
ORPort TODO1

ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy

# Replace "TODO2" with an obfs4 port of your choice.  This port must be
# externally reachable and must be different from the one specified for ORPort.
# Avoid port 9001 because it's commonly associated with
# Tor and censors may be scanning the Internet for this port.
ServerTransportListenAddr obfs4 0.0.0.0:TODO2

# Local communication port between Tor and obfs4.  Always set this to "auto".
# "Ext" means "extended", not "external".  Don't try to set a specific port
# number, nor listen on 0.0.0.0.
ExtORPort auto

# Replace "<address@email.com>" with your email address so we can contact you if
# there are problems with your bridge.  This is optional but encouraged.
ContactInfo <address@email.com>

# Pick a nickname that you like for your bridge.  This is optional.
Nickname PickANickname
```
Don't forget to change the `ORPort`, `ServerTransportListenAddr`, `ContactInfo`, and `Nickname` options.

* Note that both Tor's OR port and its obfs4 port must be reachable. If your bridge is behind a firewall or NAT, make sure to open both ports. You can use [our reachability test](https://bridges.torproject.org/scan/) to see if your obfs4 port is reachable from the Internet.

### 5. Enable and Start `tor`

```
# rc-update add tor default
# rc-service tor start
```
    ... or restart it if it was running already, so configurations take effect

```
# rc-service tor restart
```

### 6. Final Notes

To increase the number of allowed file descriptors for your bridge and prevent errors as usage increases, add `rc_ulimit="-n 20000"` to `/etc/init.d/tor`.

If you are having trouble setting up your bridge, have a look at [our help section](../../../getting-help/).
If your bridge is now running, check out the [post-install notes](../post-install/).
---
html: two-columns-page.html
---
key:
---
subtitle: How to deploy an obfs4 bridge on Alpine Linux
---
_template: layout.html
---
section: bridge
---
section_id: bridge
